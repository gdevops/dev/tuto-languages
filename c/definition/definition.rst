
.. _c_definition:

=====================
C definition
=====================

.. contents::
   :depth: 3


https://en.wikipedia.org/wiki/C_(programming_language)
=========================================================

.. seealso::

   - https://en.wikipedia.org/wiki/C_(programming_language)
   - https://fr.wikipedia.org/wiki/C_(langage)

C was originally developed at Bell Labs by Dennis Ritchie, between 1972 and 1973.

It was created to make utilities running on Unix. Later, it was applied to
re-implementing the kernel of the Unix operating system.

During the 1980s, C gradually gained popularity. Nowadays, it is one of the
most widely used programming languages, with C compilers from various vendors
available for the majority of existing computer architectures and operating
systems.

C has been standardized by the American National Standards Institute (ANSI)
since 1989 (see ANSI C) and subsequently by the International Organization
for Standardization (ISO).
