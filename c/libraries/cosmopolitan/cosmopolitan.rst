
.. index::
   pair: C ; Libraries
   ! αcτµαlly pδrταblε εxεcµταblε
   ! APE (αcτµαlly pδrταblε εxεcµταblε)

.. _cosmopolitan:

====================================================================
**cosmopolitan (your build-once run-anywhere C library)**
====================================================================


.. seealso::

   - https://x.com/JustineTunney
   - https://justine.lol/ape.html
   - https://github.com/jart
   - https://github.com/jart/cosmopolitan
   - https://github.com/jart/cosmopolitan/graphs/contributors
   - https://groups.google.com/g/cosmopolitan-libc/c/2MT7KOxfiIE
   - https://justine.lol/cosmopolitan/documentation.html
   - https://news.ycombinator.com/item?id=25556286


.. figure:: cosmopolitan.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
