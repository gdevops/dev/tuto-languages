

.. _cosmopolitan_def:

==============================
**cosmopolitan definition**
==============================


.. seealso::

   - https://x.com/JustineTunney
   - https://github.com/jart/cosmopolitan/graphs/contributors
   - https://groups.google.com/g/cosmopolitan-libc/c/2MT7KOxfiIE
   - https://justine.lol/cosmopolitan/documentation.html

.. contents::
   :depth: 3

Definition by Justine Tunney
================================

.. seealso::

   - https://groups.google.com/g/cosmopolitan-libc/c/2MT7KOxfiIE
   - https://justine.lol/
   - https://justine.lol/ape.html


I'll first introduce myself as Justine. I'm the author/curator of the cosmo
codebase.
I spent the last two years working on this project, for fun, after leaving
Google Brain.

One of the difficulties I noticed the research community encounters with
open source, is that (a) you're lucky if you get your program working on
more than one distro, and (b) any code you publish will approach read-only
on a long enough timeline (due to things like distro dependency churn).

Normally folks solve this by putting everything in a Docker container.

I felt like we deserved a better solution.

So `I came up with the actually portable executable file format <https://justine.lol/ape.html>`_
and wrote a more permissively licensed C library within a hermetically
defined build environment without dependencies that supports it.

Since then, I've dogfooded the work by writing the demo programs found
on my `website <https://justine.lol/>`_.
It was effortless to get them all running on all the major platforms.

**They're all distributed as single-file executables which are statically
linked and have no dependencies other than themselves.**
I believe each of these programs will continue working without maintenance
for a considerable amount of time.

Thus, once I was sure it worked and that Cosmopolitan was just as fun
for me to use, as it was to create, I brought it to the attention of the
world so you too can enjoy the same benefits.

Thanks for subscribing!

Definition
=============

Cosmopolitan makes C a build-once run-anywhere language, similar to Java,
except it doesn't require interpreters or virtual machines be installed
beforehand.

Cosmo provides the same portability benefits as high-level languages
like Go and Rust, but it doesn't invent a new language and you won't
need to configure a CI system to build separate binaries for each
operating system
