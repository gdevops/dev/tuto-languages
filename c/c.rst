
.. index::
   pair: C ; Language

.. _c_language:

=====================
C
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/C_(programming_language)
   - https://fr.wikipedia.org/wiki/C_(langage)
   - https://gitlab.com/gdevops

.. toctree::
   :maxdepth: 3

   definition/definition
   libraries/libraries
   versions/versions
