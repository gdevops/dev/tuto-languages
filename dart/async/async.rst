
.. index::
   pair: dart ; async
   ! dart async

.. _dart_async:

=====================
**Dart** async
=====================

- https://dart.dev/codelabs/async-await
- https://dart.dev/language/async
- https://dart.dev/tutorials/language/streams (Asynchronous programming: Streams)
