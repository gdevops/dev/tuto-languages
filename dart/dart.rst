
.. index::
   pair: dart ; Language
   ! dart

.. _dart_language:

=====================
**Dart** |dart|
=====================

- https://github.com/dart-lang/language
- https://www.dartlang.org/
- https://fr.wikipedia.org/wiki/Dart_(langage)
- https://dart.dev/
- https://dart.dev/language
- https://github.com/dart-lang#using-dart
- https://dart.dev/tools
- https://dart.dev/codelabs
- https://pub.dev/ (The official package repository for Dart and Flutter apps)
- https://pub.dev/feed.atom
- https://api.dart.dev/


.. toctree::
   :maxdepth: 3

   definition/definition
   async/async
