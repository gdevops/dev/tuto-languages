
.. index::
   pair: dart ; Language
   ! dart

.. _dart_language_def:

=====================
**Dart definition**
=====================


Wikipedia description
======================

- https://fr.wikipedia.org/wiki/Dart_(langage)


Dart est un langage de programmation optimisé pour les applications sur
plusieurs plateformes.

Il est développé par Google et est utilisé pour créer des applications
mobiles, de bureau, de serveur et web.

Dart est un langage orienté objet à ramasse-miettes avec une syntaxe de
type C++.

Dart peut se compiler en code natif ou en JavaScript.

Il prend en charge les interfaces, les mixins, les classes abstraites,
les génériques réifiés et l'inférence de type


Description
============

Dart (initialement appelé Dash) est un langage de programmation web développé
par Google.

Son but initial est de remplacer JavaScript pour devenir la nouvelle lingua
franca du développement web, néanmoins la priorité actuelle des développeurs
est que le code Dart puisse être converti en code JavaScript compatible avec
tous les navigateurs modernes, ainsi que sur le développement d'application
multi-plateforme.

Dart peut aussi être utilisé pour la programmation côté serveur, ainsi que
le développement d'applications mobiles (via l'API Flutter).


Using Dart
===================

- https://github.com/dart-lang#using-dart
- https://dart.dev/tools
- https://dart.dev/codelabs
- https://pub.dev/
- https://api.dart.dev/

Visit dart.dev to learn more about the language, tools, and to find `codelabs <https://dart.dev/codelabs>`_

Browse `pub.dev <https://pub.dev/>`_ for more packages and libraries contributed by the community
and the Dart team.

Our API reference documentation is published at `api.dart.dev <https://api.dart.dev/>`_, based on
the stable release.

(We also publish docs from our beta and dev channels, as well as from the main branch).
