

==========================
gvm install examples
==========================

.. contents::
   :depth: 3


gvm install go1.12.6
======================


::

    gvm install go1.12.6

::

    Downloading Go source...
    Installing go1.12.6...
     * Compiling...
    go1.12.6 successfully installed!

::

    (logtransactions_doc)  go --help


::

    Go is a tool for managing Go source code.

    Usage:

	    go command [arguments]

    The commands are:

	    build       compile packages and dependencies
	    clean       remove object files and cached files
	    doc         show documentation for package or symbol
	    env         print Go environment information
	    bug         start a bug report
	    fix         update packages to use new APIs
	    fmt         gofmt (reformat) package sources
	    generate    generate Go files by processing source
	    get         download and install packages and dependencies
	    install     compile and install packages and dependencies
	    list        list packages
	    run         compile and run Go program
	    test        test packages
	    tool        run specified go tool
	    version     print Go version
	    vet         report likely mistakes in packages

    Use "go help [command]" for more information about a command.

    Additional help topics:

	    c           calling between Go and C
	    buildmode   build modes
	    cache       build and test caching
	    filetype    file types
	    gopath      GOPATH environment variable
	    environment environment variables
	    importpath  import path syntax
	    packages    package lists
	    testflag    testing flags
	    testfunc    testing functions

    Use "go help [topic]" for more information about that topic.



go version
============

::

    (logtransactions_doc)  > go version
    go version go1.10.4 linux/amd64


gvm use go1.12.6 --default
==========================

::

    gvm use go1.12.6 --default

::


    Now using version go1.12.6




gvm list
=========

::

    gvm list

::

    gvm gos (installed)

       go1.12.6
       system
