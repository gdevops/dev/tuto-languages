
.. index::
   pair: Go ; Definition

.. _go_definition:

=====================
Go definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Go_(programming_language)


Go (often referred to as Golang) is a statically typed, compiled programming
language designed at Google by Robert Griesemer, Rob Pike, and Ken Thompson.

Go is syntactically similar to C, but with memory safety, garbage collection,
structural typing, and CSP-style concurrency.

There are two major implementations:

- Google's self-hosting[16] compiler toolchain targeting multiple operating
  systems, mobile devices and WebAssembly.
- gccgo, a GCC frontend.

A third compiler, GopherJS compiles Go to JavaScript for front-end web development.


History
=========

Go was designed at Google in 2007 to improve programming productivity in an
era of multicore, networked machines and large codebases.

The designers wanted to address criticism of other languages in use at Google,
but keep their useful characteristics:

- Static typing and run-time efficiency (like C++ or Java)
- Readability and usability (like Python or JavaScript)
- High-performance networking and multiprocessing

The designers were primarily **motivated by their shared dislike of C++**.

Go was publicly announced in November 2009,[28] and version 1.0 was released
in March 2012.

Go is widely used in production at Google and in many other organizations
and open-source projects.

Gopher mascot
===============

In April 2018, the original logo was replaced with a stylized GO slanting
right with trailing streamlines. However, the gopher mascot remained the same.

.. figure:: ../Go_Logo_Aqua.svg.png
   :align: center


In August 2018, the Go principal contributors published two ″draft designs″
for new language features, Generics and Error Handling, and asked Go users
to submit feedback on them.

Lack of support for generic programming and the verbosity of error handling
in Go 1.x had drawn considerable criticism.
