
.. index::
   pair: Go ; 1.14 (2020-02-25)

.. _go_1_14_0:

==========================
Go 1.14 (2020-02-25)
==========================

.. seealso::

   - https://golang.org/doc/go1.14
   - https://go.googlesource.com/go/+/refs/tags/go1.14

.. contents::
   :depth: 3

Introduction to Go 1.14
=========================

The latest Go release, version 1.14, arrives six months after Go 1.13.

Most of its changes are in the implementation of the toolchain, runtime,
and libraries.

As always, the release maintains the Go 1 promise of compatibility.

We expect almost all Go programs to continue to compile and run as before.

Module support in the go command is now ready for production use, and
we encourage all users to migrate to Go modules for dependency management.

If you are unable to migrate due to a problem in the Go toolchain,
please ensure that the problem has an open issue filed.
(If the issue is not on the Go1.15 milestone, please let us know why it
prevents you from migrating so that we can prioritize it appropriately.)
