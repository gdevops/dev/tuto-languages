
.. index::
   pair: Go ; Versions

.. _go_versions:

=====================
Go versions
=====================

.. seealso::

   - https://golang.org/doc/devel/release.html
   - https://go.googlesource.com/go
   - https://golang.org/doc/

.. toctree::
   :maxdepth: 3

   1.14.0/1.14.0
   1.13.10/1.13.10
   1.13.0/1.13.0
   1.12/1.12
   1.11/1.11
