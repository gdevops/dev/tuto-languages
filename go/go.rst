
.. index::
   pair: Go ; Language
   ! Go
   ! Golang

.. _go_language:

=====================
**Go**
=====================

- https://en.wikipedia.org/wiki/Go_(programming_language)
- https://go.googlesource.com/go
- https://golang.org/doc/
- https://x.com/golang
- https://gitlab.com/gdevops
- https://newsletter.appliedgo.net/rss
- https://codapi.org/try/go/
- https://antonz.org/

.. figure:: Go_Logo_Aqua.svg.png
   :align: center

.. figure:: Golang.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   versions/versions
