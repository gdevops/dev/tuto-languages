
.. index::
   pair: GCC ; Definition

.. _gcc_definition:

=====================
GCC definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/GNU_Compiler_Collection


The GNU Compiler Collection (GCC) is a compiler system produced by the GNU
Project supporting various programming languages.

**GCC is a key component of the GNU toolchain and the standard compiler for
most Unix-like operating systems**.

The Free Software Foundation (FSF) distributes GCC under the GNU General
Public License (GNU GPL). GCC has played an important role in the growth of
free software, as both a tool and an example.

When it was first released in 1987, GCC 1.0 was named the GNU C Compiler
since it only handled the C programming language.

It was extended to compile C++ in December of that year. Front ends were later
developed for Objective-C, Objective-C++, Fortran, Java, Ada, and Go,
among others.
