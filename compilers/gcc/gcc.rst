
.. index::
   pair: GCC ; Compiler
   ! GCC
   ! GNU Compiler Collection

.. _gcc:

===============================
GCC (GNU Compiler Collection)
===============================

.. seealso::

   - https://en.wikipedia.org/wiki/GNU_Compiler_Collection
   - https://gcc.gnu.org/
   - https://x.com/gnutools


.. figure:: gccegg-65.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
