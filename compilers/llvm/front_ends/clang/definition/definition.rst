
.. index::
   pair: Clang ; Definition

.. _clanf_def:

===============================
Clang definition
===============================

.. seealso::

   - https://clang.llvm.org/
   - https://clang.llvm.org/comparison.html

.. contents::
   :depth: 3


Definition
===========

Clang /ˈklæŋ/ is a compiler front end for the C, C++, Objective-C and
Objective-C++ programming languages, as well as the OpenMP, OpenCL, RenderScript
and CUDA frameworks.

It uses the LLVM compiler infrastructure as its back end and has been part of
the LLVM release cycle since LLVM 2.6.

It is designed to act as a drop-in replacement for the GNU Compiler Collection
(GCC), supporting most of its compilation flags and unofficial language
extensions.

Its contributors include Apple, Microsoft, Google, ARM, Sony, Intel and
Advanced Micro Devices (AMD).

It is open-source software, with source code released under the University of
Illinois/NCSA License, a permissive free software licence.

The Clang project includes the Clang front end, a static analyzer and several
code analysis tools

Why ?
=======

Development of the new front-end was started out of a need for a compiler that
allows better diagnostics, better integration with IDEs, a license that is
compatible with commercial products, and a nimble compiler that is easy to
develop and maintain. All of these were motivations for starting work on a
new front-end that could meet these needs.

For a more detailed comparison between Clang and other compilers, please see
the `Clang comparison page`_


.. _`Clang comparison page`:  https://clang.llvm.org/comparison.html
