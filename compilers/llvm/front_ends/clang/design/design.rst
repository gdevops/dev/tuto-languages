
.. index::
   pair: Clang ; Design

.. _clang_design:

===============================
Clang design
===============================

.. seealso::

   - https://en.wikipedia.org/wiki/Clang

.. contents::
   :depth: 3

Design
=======

Clang is intended to work atop LLVM.

The combination of Clang and LLVM provides most of the toolchain, to allow
replacing the full GCC stack.

Because it is built with a library-based design, like the rest of LLVM,
Clang is easy to embed into other applications.

This is one reason why most OpenCL implementations are built with Clang and
LLVM.

One of Clang's main goals is to provide a library-based architecture, to allow
the compiler to be more tightly tied to tools that interact with source code,
such as an integrated development environment (IDE) graphical user interface (GUI).

In contrast, GCC is designed to work in a compile-link-debug workflow, and
integrating it with other tools is not always easy.

For instance, GCC uses a step called fold that is key to the overall compile
process, which has the side effect of translating the code tree into a form
that looks unlike the original source code.
If an error is found during or after the fold step, it can be difficult to
translate that back into one location in the original source.

Also, vendors using the GCC stack within IDEs use separate tools to index the
code, to provide features like syntax highlighting and autocomplete.
