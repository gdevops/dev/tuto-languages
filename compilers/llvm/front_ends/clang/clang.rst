
.. index::
   pair: CLang ; Compiler
   ! Clang

.. _clang:

===========================================
Clang compiler (LLVM front end compiler)
===========================================

.. seealso::

   - https://clang.llvm.org/


.. toctree::
   :maxdepth: 3

   definition/definition
   design/design
