
.. index::
   pair: Front-end ; Compiler

.. _compilers_front_end:

===============================
Compilers front-end
===============================

.. contents::
   :depth: 3

Introduction
============

.. seealso::

   - https://en.wikipedia.org/wiki/LLVM#Front_ends

LLVM was originally written to be a replacement for the existing code generator
in the GCC stack, and many of the GCC front ends have been modified to work with
it.

LLVM currently supports compiling of Ada, C, C++, D, Delphi, Fortran, Haskell,
Julia, Objective-C, Rust, and Swift using various front ends, some derived from
version 4.0.1 and 4.2 of the GNU Compiler Collection (GCC).

Widespread interest in LLVM has led to several efforts to develop new front
ends for a variety of languages.

The one that has received the most attention is :ref:`Clang <clang>`, a new
compiler supporting C, C++, and Objective-C.

Primarily supported by Apple, Clang is aimed at replacing the C/Objective-C
compiler in the GCC system with a system that is more easily integrated with
integrated development environments (IDEs) and has wider support for
multithreading. Support for OpenMP directives has been included in Clang
since release 3.8.

The Utrecht Haskell compiler can generate code for LLVM. Though the generator
is in the early stages of development, in many cases it has been more efficient
than the C code generator.

The Glasgow Haskell Compiler (GHC) has a working LLVM backend that achieves
a 30% speed-up of the compiled code relative to native code compiling via
GHC or C code generation followed by compiling, missing only one of the
many optimizing techniques implemented by the GHC.

Many other components are in various stages of development, including, but not
limited to, the Rust compiler, a Java bytecode front end, a Common Intermediate
Language (CIL) front end, the MacRuby implementation of Ruby 1.9,
various front ends for Standard ML, and a new graph coloring register allocator.

Clang
======


.. toctree::
   :maxdepth: 3

   clang/clang
