
.. index::
   pair: LLVM ; Definition

.. _llvm_definition:

=====================
LLVM definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/LLVM


The LLVM compiler infrastructure project is a "collection of modular and
reusable compiler and toolchain technologies" used to develop compiler
front ends and back ends.

LLVM is written in C++ and is designed for compile-time, link-time, run-time,
and "idle-time" optimization of programs written in arbitrary programming
languages.

Originally implemented for C and C++, the language-agnostic design of LLVM has
since spawned a wide variety of front ends: languages with compilers that use
LLVM include ActionScript, Ada, C#, Common Lisp, Crystal, CUDA, D, Delphi, Dylan,
Fortran, Graphical G Programming Language, Halide, Haskell, Java bytecode,
Julia, Kotlin, Lua, Objective-C, OpenGL Shading Language, Pony, Python, R,
Ruby, Rust, Scala, Swift, and Xojo.


History
========

The LLVM project started in 2000 at the University of Illinois at Urbana–Champaign,
under the direction of Vikram Adve and Chris Lattner.
LLVM was originally developed as a research infrastructure to investigate
dynamic compilation techniques for static and dynamic programming languages.

LLVM was released under the University of Illinois/NCSA Open Source License,
a permissive free software licence. In 2005, Apple Inc. hired Lattner and
formed a team to work on the LLVM system for various uses within Apple's
development systems.

LLVM is an integral part of Apple's latest development tools for macOS and iOS.

Since 2013, Sony has been using LLVM's primary front end Clang compiler in the
software development kit (SDK) of its PlayStation 4 console.

The name LLVM was originally an initialism for Low Level Virtual Machine.
This initialism has officially been removed to avoid confusion, as the LLVM
has evolved into an umbrella project that has little relationship to what most
current developers think of as virtual machines.

Now, LLVM is a brand that applies to the LLVM umbrella project, the LLVM
intermediate representation (IR), the LLVM debugger, the LLVM implementation
of the C++ Standard Library (with full support of C++11 and C++14), etc.

LLVM is administered by the LLVM Foundation.
Its president is compiler engineer Tanya Lattner.
