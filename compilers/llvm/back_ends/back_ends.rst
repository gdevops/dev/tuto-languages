
.. index::
   pair: Back-end; Compiler

.. _compilers_back_end:

===============================
Compilers back-end
===============================

.. contents::
   :depth: 3

Introduction
============

.. seealso::

   - https://en.wikipedia.org/wiki/LLVM#Front_ends


At version 3.4, LLVM supports many instruction sets, including ARM, Qualcomm
Hexagon, MIPS, Nvidia Parallel Thread Execution (PTX; called NVPTX in LLVM
documentation), PowerPC, AMD TeraScale, AMD Graphics Core Next (GCN), SPARC,
z/Architecture (called SystemZ in LLVM documentation), x86, x86-64, and XCore.

Some features are not available on some platforms.

Most features are present for x86, x86-64, z/Architecture, ARM, and PowerPC.

RISC-V is supported as of version 7.

The LLVM machine code (MC) subproject is LLVM's framework for translating
machine instructions between textual forms and machine code.
Formerly, LLVM relied on the system assembler, or one provided by a
toolchain, to translate assembly into machine code.

LLVM MC's integrated assembler supports most LLVM targets, including x86,
x86-64, ARM, and ARM64. For some targets, including the various MIPS instruction
sets, integrated assembly support is usable but still in the beta stage.

emscripten
============


.. toctree::
   :maxdepth: 3

   emscripten/emscripten
