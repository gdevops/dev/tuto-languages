
.. index::
   pair: Back-end; emscripten

.. _emscripten:

===============================
emscripten
===============================

.. seealso::

   - https://en.wikipedia.org/wiki/Emscripten
   - https://emscripten.org/


.. figure:: Emscripten_logo_full.png
   :align: center

.. contents::
   :depth: 3

.. toctree::
   :maxdepth: 3

   definition/definition
