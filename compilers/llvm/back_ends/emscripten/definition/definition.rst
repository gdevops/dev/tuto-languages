

.. _emscripten_def:

===============================
emscripten definition
===============================

.. seealso::

   - https://en.wikipedia.org/wiki/Emscripten
   - https://emscripten.org/
   - https://github.com/emscripten-core/emscripten

.. contents::
   :depth: 3

Wikipedia definition
======================

.. seealso::

   - https://en.wikipedia.org/wiki/Emscripten

Emscripten is a source-to-source compiler that runs as a back end to the LLVM
compiler and produces a subset of JavaScript known as asm.js.
**It can also produce WebAssembly**.

This allows applications and libraries originally designed to run as standard
executables to be integrated into client side web applications.

asm.js can be compiled by browsers ahead of time meaning that the compiled
programs can run much faster than those traditionally written in JavaScript.

Emscripten has been used to port, among other things, Unreal Engine 3,
SQLite, MeshLab,Bullet physics, AutoCAD, and the Qt application framework.


Emscripten definition
=======================

.. seealso::

   - https://emscripten.org/

Emscripten is a toolchain for compiling to asm.js and WebAssembly, built using
LLVM, that lets you run C and C++ on the web at near-native speed without plugins.

Porting
--------

Compile your existing projects written in C or C++ and run them on all modern
browsers.


Emscripten core definition
==============================

.. seealso::

   - https://github.com/emscripten-core/emscripten

Emscripten is an LLVM-to-JavaScript compiler.

It takes LLVM bitcode - which can be generated from C/C++, using llvm-gcc
(DragonEgg) or clang, or any other language that can be converted into LLVM -
and compiles that into JavaScript, which can be run on the web (or anywhere
else JavaScript can run).
