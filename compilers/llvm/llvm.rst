
.. index::
   pair: LLVM ; Compiler
   ! LLVM

.. _llvm:

===============================
LLVM
===============================

.. seealso::

   - https://en.wikipedia.org/wiki/LLVM
   - https://llvm.org/
   - https://x.com/llvmorg
   - https://x.com/llvmweekly


.. figure:: DragonFull.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   front_ends/front_ends
   back_ends/back_ends
   versions/versions
