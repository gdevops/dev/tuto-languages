
.. index::
   pair: Ruby ; Versions

.. _ruby_versions:

=====================
Ruby versions
=====================

.. seealso::

   - https://github.com/ruby/ruby
   - https://github.com/ruby/ruby/releases
   - https://en.wikipedia.org/wiki/Ruby_(programming_language)#History

.. toctree::
   :maxdepth: 3

   2.6.3/2.6.3
   2.6.0/2.6.0
