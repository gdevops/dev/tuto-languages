
.. index::
   pair: Ruby ; Language

.. _ruby_language:

=====================
Ruby
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Ruby_(programming_language)
   - https://github.com/ruby/ruby
   - https://www.ruby-lang.org/en/
   - https://gitlab.com/gdevops
   - :ref:`install_ruby`


.. figure:: Ruby_logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   rvm/rvm
   rubygems/rubygems
