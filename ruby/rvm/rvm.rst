
.. index::
   pair: Ruby ; RVM
   ! rvm

.. _rvm:

============================
RVM (Ruby Version Manager)
============================

.. seealso::

   - https://rvm.io/
   - https://github.com/rvm

.. figure:: logo_rvm.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   installation/installation
