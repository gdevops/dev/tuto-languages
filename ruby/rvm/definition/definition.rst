
.. _rvm_def:

============================
RVM definition
============================

.. contents::
   :depth: 3


https://rvm.io/ definition
============================

.. seealso::

   - https://rvm.io/

RVM is a command-line tool which allows you to easily install, manage, and work
with multiple ruby environments from interpreters to sets of gems.


rvm
=====

::


    Usage:

        rvm [--debug][--trace][--nice] <command> <options>

      for example:

        rvm list                # list installed interpreters
        rvm list known          # list available interpreters
        rvm install <version>   # install ruby interpreter
        rvm use <version>       # switch to specified ruby interpreter
        rvm remove <version>    # remove ruby interpreter
        rvm get <version>       # upgrade rvm: stable, master

    Available commands:

      rvm has a number of common commands, listed below. Additional information about any command
      can be found by executing `rvm help <command>`.

      ruby installation
          fetch                   # download binary or sources for selected ruby version
          install                 # install ruby interpreter
          list                    # show currently installed ruby interpreters
          list known              # list available interpreters
          mount                   # install ruby from external locations
          patchset                # tools related to managing ruby patchsets
          pkg                     # install a dependency package
          reinstall               # reinstall ruby and run gem pristine on all gems
          remove                  # remove ruby and downloaded sources
          requirements            # installs dependencies for building ruby
          uninstall               # uninstall ruby, keeping it's sources
          upgrade                 # upgrade to another ruby version, migrating gems

      running different ruby versions
          current                 # print current ruby version and name of used gemsets


rvm list
==========

::

    =* ruby-2.6.0 [ x86_64 ]

    # => - current
    # =* - current && default
    #  * - default
