
.. index::
   pair: RVM ; installation

.. _rvm_installation:

============================
RVM installation
============================

.. seealso::

   - https://rvm.io/
   - https://rvm.io/rvm/install

.. contents::
   :depth: 3


Cut Rubies with ease !
======================

Install GPG keys:

::

    gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB


::

    \curl -sSL https://get.rvm.io | bash -s stable

::

    Downloading https://github.com/rvm/rvm/archive/1.29.7.tar.gz
    Downloading https://github.com/rvm/rvm/releases/download/1.29.7/1.29.7.tar.gz.asc

    Installing RVM to /home/pvergain/.rvm/
        Adding rvm PATH line to /home/pvergain/.profile /home/pvergain/.mkshrc /home/pvergain/.bashrc /home/pvergain/.zshrc.
        Adding rvm loading line to /home/pvergain/.profile /home/pvergain/.bash_profile /home/pvergain/.zlogin.
    Installation of RVM in /home/pvergain/.rvm/ is almost complete:

      * To start using RVM you need to run `source /home/pvergain/.rvm/scripts/rvm`
        in all your open shell windows, in rare cases you need to reopen all shell windows.



rvm list
=========

::

    # No rvm rubies installed yet. Try 'rvm help install'.


.. _install_ruby:

rvm install ruby
===================

::

    rvm install ruby

::


    Searching for binary rubies, this might take some time.
    No binary rubies available for: mint/19.1/x86_64/ruby-2.6.0.
    Continuing with compilation. Please read 'rvm help mount' to get more information on binary rubies.
    Checking requirements for mint.
    Installing requirements for mint.
    Updating system - please wait
    pvergain password required for 'apt-get --quiet --yes update':
    Installing required packages: libncurses5-dev, libyaml-dev, sqlite3, libgmp-dev, libreadline-dev - please wait
    Requirements installation successful.
    __rvm_install_source:source:2: aucun fichier ou dossier de ce type: /home/pvergain/.rvm/scripts/functions/manage/install/mint
    Installing Ruby from source to: /home/pvergain/.rvm/rubies/ruby-2.6.0, this may take a while depending on your cpu(s)...
    ruby-2.6.0 - #downloading ruby-2.6.0, this may take a while depending on your connection...
      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100 13.9M  100 13.9M    0     0  4815k      0  0:00:02  0:00:02 --:--:-- 4815k
    ruby-2.6.0 - #extracting ruby-2.6.0 to /home/pvergain/.rvm/src/ruby-2.6.0 - please wait
    ruby-2.6.0 - #configuring - please wait
    ruby-2.6.0 - #post-configuration - please wait
    ruby-2.6.0 - #compiling - please wait
    ruby-2.6.0 - #installing - please wait
    ruby-2.6.0 - #making binaries executable - please wait
    Installed rubygems 3.0.1 is newer than 2.7.9 provided with installed ruby, skipping installation, use --force to force installation.
    ruby-2.6.0 - #gemset created /home/pvergain/.rvm/gems/ruby-2.6.0@global
    ruby-2.6.0 - #importing gemset /home/pvergain/.rvm/gemsets/global.gems - please wait
    ruby-2.6.0 - #generating global wrappers - please wait
    ruby-2.6.0 - #gemset created /home/pvergain/.rvm/gems/ruby-2.6.0
    ruby-2.6.0 - #importing gemsetfile /home/pvergain/.rvm/gemsets/default.gems evaluated to empty gem list
    ruby-2.6.0 - #generating default wrappers - please wait
    ruby-2.6.0 - #adjusting #shebangs for (gem irb erb ri rdoc testrb rake).
    Install of ruby-2.6.0 - #complete
    Ruby was built without documentation, to build it run: rvm docs generate-ri


rvm docs generate-ri
=======================

::

    rvm docs generate-ri


::


    Generating ri documentation.....................................................
    ................................................................................
    .......................................



whereis gem
=============

::

    whereis gem

::


    gem: /usr/bin/gem /usr/bin/gem2.5 /usr/bin/gem2.3
    /home/pvergain/.rvm/rubies/ruby-2.6.0/bin/gem /usr/share/man/man1/gem.1.gz


.. _gem:

gem
=====

::

    gem


::

    RubyGems is a sophisticated package manager for Ruby.
    This is a basic help message containing pointers to more information.

      Usage:
        gem -h/--help
        gem -v/--version
        gem command [arguments...] [options...]

      Examples:
        gem install rake
        gem list --local
        gem build package.gemspec
        gem help install

      Further help:
        gem help commands            list all 'gem' commands
        gem help examples            show some examples of usage
        gem help gem_dependencies    gem dependencies file guide
        gem help platforms           gem platforms guide
        gem help <COMMAND>           show help on COMMAND
                                       (e.g. 'gem help install')
        gem server                   present a web page at
                                     http://localhost:8808/
                                     with info about installed gems
      Further information:
        http://guides.rubygems.org
