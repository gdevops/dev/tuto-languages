
.. index::
   pair: 1.29.7 (2019-01-03) ; rvm

.. _rvm_1_29_7:

============================
RVM 1.29.7 (2019-01-03)
============================

.. seealso::

   - https://github.com/rvm/rvm/tree/1.29.7
   - https://github.com/rvm/rvm/releases/tag/1.29.7
   - https://github.com/rvm/rvm/compare/1.29.6...1.29.7

.. contents::
   :depth: 3

New features
=============

- Set Ruby 2.6.0 as the default Ruby version #4544
- RailsExpress patches for 2.6.0 #4547
- Add support for gcc@8 with Homebrew and drop gcc < 4.9 #4556

New interpreters
=================

Add support for Ruby 2.6.0-rc2 #4526 and 2.6.0 #4542
