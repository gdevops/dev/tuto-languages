
.. index::
   pair: Ruby ; gems
   ! gems

.. _rubygems:

=====================
Rubygems
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/RubyGems
   - https://github.com/rubygems/rubygems
   - http://www.rubydoc.info/github/rubygems/rubygems
   - https://rubygems.org/
   - http://guides.rubygems.org
   - :ref:`install_ruby`


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
