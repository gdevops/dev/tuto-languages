
.. _languages_others:

=====================
Others
=====================

.. toctree::
   :maxdepth: 6

   c_sharp/c_sharp
   c_plusplus/c_plusplus
   elixir/elixir
   groovy/groovy
   java/java
   kotlin/kotlin
   lua/lua
   perl/perl
   php/php
   swift/swift
   webassembly/webassembly
