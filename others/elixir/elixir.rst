
.. index::
   pair: Elixir ; Language
   ! Elixir

.. _elixir:

=====================
Elixir
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Elixir_(langage)
   - https://github.com/elixir-lang/elixir
   - https://elixir-lang.org/
   - https://gitlab.com/gdevops


.. figure:: Elixir_programming_language_logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
