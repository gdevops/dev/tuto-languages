
.. index::
   pair: Elixir ; Versions

.. _elixir_versions:

=====================
Elixir versions
=====================

.. seealso::

   - https://github.com/elixir-lang/elixir/releases
   - https://github.com/elixir-lang/elixir/blob/master/CHANGELOG.md


.. toctree::
   :maxdepth: 3

   1.8.1/1.8.1
