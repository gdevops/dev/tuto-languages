
.. _elixir_def:

=====================
Elixir definition
=====================

.. contents::
   :depth: 3


Elixir definition
==================

.. seealso::

   - https://elixir-lang.org/

Elixir is a dynamic, functional language designed for building scalable and
maintainable applications.

Elixir leverages the Erlang VM, known for running low-latency, distributed
and fault-tolerant systems, while also being successfully used in web
development and the embedded software domain.


English wikipedia definition
==============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Elixir_(langage)


Elixir is a functional, concurrent, general-purpose programming language that
runs on the Erlang virtual machine (BEAM).

Elixir builds on top of Erlang and shares the same abstractions for building
distributed, fault-tolerant applications.

Elixir also provides productive tooling and an extensible design. The latter
is supported by compile-time metaprogramming with macros and polymorphism
via protocols.
