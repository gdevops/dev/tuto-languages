
.. index::
   pair: Language ; Groovy

.. _groovy_language:

=====================
Groovy language
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_Groovy
   - http://groovy-lang.org/


.. figure:: Groovy-logo.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
