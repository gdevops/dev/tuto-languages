
.. index::
   pair: Java ; Language

.. _java_language:

=====================
Java language
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Java_(programming_language)
   - http://hg.openjdk.java.net/jdk
   - https://www.oracle.com/java/
   - https://x.com/OpenJDK
   - https://x.com/parisjug
   - https://x.com/LyonJUG
   - https://x.com/jimbaker
   - https://gitlab.com/gdevops


.. figure:: Java_programming_language_logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
