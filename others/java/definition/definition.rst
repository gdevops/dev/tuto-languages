
.. index::
   pair: Java ; Definition

.. _java_definition:

=====================
Java definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Ruby_(programming_language)


Java is a general-purpose computer-programming language that is concurrent,
class-based, object-oriented, and specifically designed to have as few
implementation dependencies as possible.

It is intended to let application developers "write once, run anywhere" (WORA),
meaning that compiled Java code can run on all platforms that support Java
without the need for recompilation.

Java applications are typically compiled to "bytecode" that can run on any
Java virtual machine (JVM) regardless of the underlying computer architecture.

The language derives much of its original features from SmallTalk, with a
syntax similar to C and C++, but it has fewer low-level facilities than either
of them.
As of 2016, Java was one of the most popular programming languages in use,
particularly for client-server web applications, with a reported 9 million
developers.

History
========

Java was originally developed by a Canadian James Gosling at Sun Microsystems
(which has since been acquired by Oracle) and released in 1995 as a core
component of Sun Microsystems' Java platform.
The original and reference implementation Java compilers, virtual machines, and
class libraries were originally released by Sun under proprietary licenses.

As of May 2007, in compliance with the specifications of the Java Community
Process, Sun had relicensed most of its Java technologies under the GNU General
Public License. Meanwhile, others have developed alternative implementations
of these Sun technologies, such as the GNU Compiler for Java (bytecode compiler),
GNU Classpath (standard libraries), and IcedTea-Web (browser plugin for applets).
