.. index::
   pair: webassembly ; Language
   ! WASM
   ! WebAssembly

.. _ref_webassembly_language:

==================================
**Webassembly language** (WASM)
==================================

- :ref:`tuto_wasm:wasm_tuto`
