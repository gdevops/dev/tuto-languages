
.. index::
   pair: Versions ; PHP

.. _php_versions:

=============================
PHP versions
=============================

.. seealso::

   - https://github.com/php/php-src/releases

.. toctree::
   :maxdepth: 3

   7.3.5/7.3.5
