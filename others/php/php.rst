
.. index::
   pair: Language ; PHP

.. _php:

=============================
PHP Hypertext Preprocessor
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/PHP
   - https://github.com/php/php-src
   - https://x.com/official_php


.. figure:: logo_Elephpant.svg.png
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 3

   versions/versions
