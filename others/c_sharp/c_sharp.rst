
.. index::
   pair: C♯ ; Language

.. _csharp_language:

=====================
C♯ language
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/C_Sharp_(programming_language)
   - https://gitlab.com/gdevops


.. contents::
   :depth: 3

Description
============

C♯ (pronounced C sharp) is a general-purpose, multi-paradigm programming
language encompassing strong typing, lexically scoped, imperative, declarative,
functional, generic, object-oriented (class-based), and component-oriented
programming disciplines.

It was developed around 2000 by Microsoft within its .NET initiative and later
approved as a standard by Ecma (ECMA-334) and ISO (ISO/IEC 23270:2018).

C♯ is one of the programming languages designed for the Common Language
Infrastructure.

C♯ was designed by Anders Hejlsberg, and its development team is currently l
ed by Mads Torgersen.

The most recent version is C♯ 7.3, which was released in 2018 alongside Visual
Studio 2017 version 15.7.2


Versions
=========

.. toctree::
   :maxdepth: 3

   versions/versions
