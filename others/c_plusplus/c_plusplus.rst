
.. index::
   pair: C++ ; Language

.. _cplusplus_language:

=====================
C++ language
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/C%2B%2B
   - https://isocpp.org/
   - https://x.com/isocpp
   - https://gitlab.com/gdevops


.. figure:: ISO_C++_Logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
