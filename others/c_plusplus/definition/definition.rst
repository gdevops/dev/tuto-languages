
.. index::
   pair: C++ ; Definition

.. _cplusplus_definition:

=====================
C++ definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/C%2B%2B


C++ est un langage de programmation compilé permettant la programmation sous de
multiples paradigmes (comme la programmation procédurale, orientée objet ou
générique).

Ses bonnes performances, et sa compatibilité avec le C en font un des langages
de programmation les plus utilisés dans les applications où la performance est
critique.

Créé initialement par Bjarne Stroustrup dans les années 1980, le langage C++
est aujourd'hui normalisé par l'ISO.

Sa première normalisation date de 1998 (ISO/CEI 14882:1998), ensuite amendée
par l'erratum technique de 2003 (ISO/CEI 14882:2003).

Une importante mise à jour a été ratifiée et publiée par l'ISO en septembre 2011
 sous le nom de ISO/IEC 14882:2011, ou C++11.

 Depuis, des mises à jour sont publiées régulièrement : en 2014 (ISO/CEI 14882:2014,
 ou C++14) puis en 2017 (ISO/CEI 14882:2017, ou C++17).
