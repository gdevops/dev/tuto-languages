
.. index::
   pair: Language ; Lua

.. _lua_language:

=====================
Lua language
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lua


.. figure:: Lua-logo-nolabel.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
