

.. _lua_def:

=====================
Lua language
=====================

.. contents::
   :depth: 3

French wikipedia definition
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lua

Lua est un langage de script libre, réflexif et impératif.

Créé en 1993, il est conçu de manière à pouvoir être embarqué au sein
d'autres applications afin d'étendre celles-ci. Lua (du portugais :
Lua [ˈlu.ɐ], au Brésil : [ˈlu.a], signifiant « Lune »3) a été développé
par Luiz Henrique de Figueiredo, Roberto Ierusalimschy (en) et
Waldemar Celes, membres du groupe de recherche TeCGraf, de l'université
pontificale catholique de Rio de Janeiro au Brésil.

L'interpréteur Lua est écrit en langage C ANSI strict, et de ce fait
est compilable sur une grande variété de systèmes.

Il est également très compact, la version 5.0.2 n'occupant que 95 ko à
185 ko selon le compilateur utilisé et le système cible.

Il est souvent utilisé dans des systèmes embarqués tels qu'OpenWrt4
où cette compacité est très appréciée. Il profite de la compatibilité
que possède le langage C avec un grand nombre de langages pour
s'intégrer facilement dans la plupart des projets.

Il est particulièrement apprécié pour l'embarqué, le développement
réseau et les jeux vidéo.

Le Lua est utilisé dans des jeux vidéo comme : World Of Warcraft
(addons), roblox, garrysmod, computercraft mod., Multi Theft Auto
(mod multijoueur de GTA: San Andreas), ainsi que les moteurs de jeu
vidéo tels que CryENGINE, LÖVE, ou encore les Fantasy consoles.

Il est également utilisé au niveau réseau comme hook, Apache, Lighttpd
(par défaut), Nginx (via OpenResty), dans les routeurs Cisco,
dans l'analyseur de paquets Wireshark, l'antispam Rspamd,
l'autocommutateur téléphonique privé Asterisk (optionnel), pour les
scripts de MediaWiki.
