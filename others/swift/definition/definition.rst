

.. _swift_def:

=====================
Swift definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

- https://en.wikipedia.org/wiki/Swift_(programming_language)


Swift is a general-purpose, multi-paradigm, compiled programming language
developed by Apple Inc. for iOS, macOS, watchOS, tvOS, Linux, and z/OS.

Swift is designed to work with Apple's Cocoa and Cocoa Touch frameworks
and the large body of existing Objective-C code written for Apple
products.

It is built with the open source LLVM compiler framework and has been
included in Xcode since version 6, released in 2014.

On Apple platforms, it uses the Objective-C runtime library which
allows C, Objective-C, C++ and Swift code to run within one program
