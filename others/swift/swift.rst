
.. index::
   pair: Language ; Swift

.. _swift_language:

=============================
|swift| Swift language
=============================

- https://fr.wikipedia.org/wiki/Swift_(langage_d%27Apple)
- https://en.wikipedia.org/wiki/Swift_(programming_language)
- https://swift.org/


.. figure:: Swift_logo_with_text.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
