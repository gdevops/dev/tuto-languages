
.. index::
   pair: Language ; Kotlin

.. _kotlin_language:

=====================
Kotlin language
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Kotlin_(programming_language)
   - https://kotlinlang.org/
   - https://github.com/JetBrains/kotlin


.. figure:: Kotlin-logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
