
.. index::
   pair: Versions ; Kotlin

.. _kotlin_versions:

=====================
Kotlin versions
=====================


.. seealso::

   - https://github.com/JetBrains/kotlin/releases


.. toctree::
   :maxdepth: 3

   1.3.30/1.3.30
