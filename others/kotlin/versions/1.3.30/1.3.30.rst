

.. index::
   pair: Kotlin; 1.3.30  (2019-04-11)

.. _kotlin_1_3_30:

============================
Kotlin 1.3.30 (2019-04-11)
============================

.. seealso::

   - https://github.com/JetBrains/kotlin/tree/v1.3.30
   - https://blog.jetbrains.com/kotlin/2019/04/kotlin-1-3-30-released/
   - https://kotlin.developpez.com/actu/260161/Android-Kotlin-est-desormais-le-langage-prefere-et-recommande-par-Google-vers-la-fin-de-Java-pour-le-developpement-Android/



Hier, lors de la première journée de l'édition 2019 de sa conférence Google I/O
dédiée aux développeurs, le géant de la recherche en ligne a annoncé que le
langage de programmation Kotlin est désormais son langage préféré pour les
développeurs d'applications Android.

«Le développement d’Android deviendra de plus en plus Kotlin-first», a écrit
Google dans un billet de blog.

«De nombreuses nouvelles API Jetpack et fonctionnalités seront d'abord proposées
pour Kotlin. Si vous commencez un nouveau projet, vous devriez l’écrire en Kotlin»,

poursuit Google en expliquant que « le code écrit en Kotlin signifie souvent
beaucoup moins de code pour vous - moins de code à taper, tester et maintenir»


Le support officiel de Kotlin a contribué à l'adoption croissante du langage
dans le monde du développement Android, au point où certains prédisaient que le
langage de JetBrains allait très vite détrôner Java.

En tout cas, cette éventualité n'est pas à écarter, car selon Google,
« plus de 50 % des développeurs professionnels Android utilisent maintenant Kotlin ».
