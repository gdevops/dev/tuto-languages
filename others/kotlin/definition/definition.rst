
.. _kotlin_def:

=====================
Kotlin definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
======================

.. seealso::

   - https://en.wikipedia.org/wiki/Kotlin_(programming_language)

Kotlin is a cross-platform, statically typed, general-purpose programming
language with type inference.

Kotlin is designed to interoperate fully with Java, and the JVM version
of its standard library depends on the Java Class Library, but type
inference allows its syntax to be more concise.

Kotlin mainly targets the JVM, but also compiles to JavaScript or
native code (via LLVM).

Kotlin is sponsored by JetBrains and Google through the Kotlin Foundation.

Kotlin is officially supported by Google for mobile development on Android.

Since the release of Android Studio 3.0 in October 2017, Kotlin is
included as an alternative to the standard Java compiler.

The Android Kotlin compiler lets the user choose between targeting
Java 6 or Java 8 compatible bytecode.

"developpez" definition
=========================

.. seealso::

   - https://kotlin.developpez.com/actu/260161/Android-Kotlin-est-desormais-le-langage-prefere-et-recommande-par-Google-vers-la-fin-de-Java-pour-le-developpement-Android/

Kotlin est un langage de programmation orienté objet et fonctionnel, avec un
typage statique qui permet de compiler pour la machine virtuelle Java et
JavaScript.

Il est développé par une équipe de programmeurs chez JetBrains, l'éditeur
d'IntelliJ IDEA, l'environnement de développement intégré pour Java et sur
lequel est basé Android Studio, l’EDI officiel pour développer les applications
Android.
