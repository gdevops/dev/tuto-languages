
.. index::
   pair: Perl ; Versions

.. _perl_versions:

=====================
Perl versions
=====================


.. toctree::
   :maxdepth: 3


   6.0/6.0
   5.0/5.0
