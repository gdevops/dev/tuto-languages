

.. _perl_def:

=====================
Perl definition
=====================

.. contents::
   :depth: 3


English wikipedia definition
==============================


.. seealso::

   - https://en.wikipedia.org/wiki/Perl_(langage)

Perl is a family of two high-level, general-purpose, interpreted, dynamic
programming languages.

"Perl" usually refers to Perl 5, but it may also refer to its redesigned
"sister language", Perl 6.

Though Perl is not officially an acronym, there are various backronyms in
use, including "Practical Extraction and Reporting Language".

Perl was originally developed by Larry Wall in 1987 as a general-purpose
Unix scripting language to make report processing easier.
Since then, it has undergone many changes and revisions.

Perl 6, which began as a redesign of Perl 5 in 2000, eventually evolved into
a separate language.
Both languages continue to be developed independently by different
development teams and liberally borrow ideas from one another.

The Perl languages borrow features from other programming languages including
C, shell script (sh), AWK, and sed;
Wall also alludes to BASIC and Lisp in the introduction to Learning Perl (
Schwartz & Christiansen) and so on. They provide text processing facilities
without the arbitrary data-length limits of many contemporary Unix commandline
tools, facilitating manipulation of text files.

Perl 5 gained widespread popularity in the late 1990s as a CGI scripting
language, in part due to its then unsurpassed regular expression and
string parsing abilities.

In addition to CGI, Perl 5 is used for system administration, network
programming, finance, bioinformatics, and other applications, such as for GUIs.

It has been nicknamed "the Swiss Army chainsaw of scripting languages"
because of its flexibility and power, and also its **ugliness**.

In 1998, it was also referred to as the "duct tape that holds the Internet
together," in reference to both its ubiquitous use as a glue language and
its perceived inelegance.


French wikipedia definition
==============================


.. seealso::

   - https://fr.wikipedia.org/wiki/Perl_(langage)



Perl est un langage de programmation créé par Larry Wall en 1987 pour traiter
facilement de l'information de type textuel.

Ce langage, interprété, s'inspire des structures de contrôle et d'impression
du langage C, mais aussi de langages de scripts sed, awk et shell (sh).

Il prend en charge les expressions régulières dans sa syntaxe même, permettant
ainsi directement des actions sur l'aspect général de séquences de texte.

Une association, The Perl Foundation, s'occupe de son devenir, et entre autres
de son éventuel passage de la version 5.x à la version 6.

Le statut du langage est celui de logiciel libre, distribué sous double
licence : Artistic License et GPL.
