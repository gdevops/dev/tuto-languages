
.. index::
   pair: Language ; Perl


.. _perl:

=====================
Perl language
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Perl_(langage)
   - https://perl5.git.perl.org/perl.git



.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
